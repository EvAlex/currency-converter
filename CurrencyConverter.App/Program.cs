﻿using System;

namespace CurrencyConverter.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            decimal from = ParseFrom(args);
            decimal rate = ParseRate(args);

            decimal result = Convert(from, rate);

            Console.WriteLine(result);
        }

        private static decimal Convert(decimal from, decimal rate)
        {
            return from * rate;
        }

        private static decimal ParseRate(string[] args)
        {
            string rateStr = "";

            foreach (string arg in args)
            {
                if (arg.StartsWith("--rate="))
                {
                    rateStr = arg.Replace("--rate=", "");
                    break;
                }
            }

            return decimal.Parse(rateStr);
        }

        private static decimal ParseFrom(string[] args)
        {
            string fromStr = "";

            foreach (string arg in args)
            {
                if (!arg.StartsWith("--rate"))
                {
                    fromStr = arg;
                    break;
                }
            }

            return decimal.Parse(fromStr);
        }
    }
}
