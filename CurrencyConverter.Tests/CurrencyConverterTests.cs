using System;
using Xunit;
using CurrencyConverter.App;
using System.IO;

namespace CurrencyConverter.Tests
{
    public class CurrencyConverterTests
    {
        [Fact]
        public void HappyPathTest()
        {
            //  arrange
            string[] args = new string[] { "--rate=100", "123" };
            var output = new StringWriter();
            Console.SetOut(output);

            //  act
            Program.Main(args);

            //  assert
            Assert.Equal("12300" + Environment.NewLine, output.ToString());
        }
    }
}
